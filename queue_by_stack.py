class Stack(object):
    def __init__(self):
        self.data = []
    
    def push(self, value):
        self.data.append(value)

    def pop(self):
        return self.data.pop()

    def __repr__(self):
        return repr(self.data)    

class Queue(object):
    def __init__(self):
        self.out = Stack()
        self.in_ = Stack()
        self.length = 0
        
    def add(self, value):
        if len(self.out.data) == 0:
            self.out.push(value)
        else:
            self.in_.push(value)
        self.length += 1
            
    def get(self):
        if self.length == 0:
            raise ValueError("Get from empty queue")     
        ret_value = self.out.pop()
        self.length -= 1
        
        for _ in range(self.length - 1):
            self.out.push(self.in_.pop())
        if self.length > 0:  
            next_ret = self.in_.pop()
            for _ in range(self.length - 1):
                self.in_.push(self.out.pop())
            self.out.push(next_ret)
        return ret_value
    
    def __repr__(self):
        return "Q: %s %s" % (self.out, self.in_)


if __name__ == "__main__":
    while True:
        import random
        l = random.randint(1, 20)
        data = [random.randint(0, 100) for _ in range(l)]
        q = Queue()
        for el in data:
            q.add(el)
        data2 = [q.get() for _ in range(q.length)]
        assert data == data2