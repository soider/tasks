import collections

def note(mag, text):
    words_in_text = collections.Counter(text)
    words_in_mag = collections.Counter(text)
    for word, count in words_in_text.items():
        if word not in words_in_mag:
            return False
        if words_in_mag[word] < count:
            return False
    return True

assert note("h ghq g xxy wdnr anjst xxy wdnr h h anjst wdnr".split(), "h ghq".split()) == True