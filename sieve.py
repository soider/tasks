import math


def sieve(n):
    variants = range(2, n)
    mask = [True] * len(variants)
    for j, x in enumerate(variants):
        if j >= math.sqrt(n) + 2:
            break
        for idx, is_possible in enumerate(mask):
            if not is_possible:
                continue
            if variants[idx] != x and variants[idx] % x == 0:
                mask[idx] = False
                continue
    return [v for (idx, v) in enumerate(variants) if mask[idx]]


print sieve(12500000)
