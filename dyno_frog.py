import sys


def print_matrix(matrix):
    for row in matrix:
        for el in row:
            print el,
        print

def print_path(matrix):
    """Sub-optimal decision"""
    path = [(0, 0)]
    print_matrix(matrix)

    def get_available(y, x):
        result = []
        for point in  [
            (y+1, x),
            (y, x+1)
        ]:
            y,x = point
            try:
                matrix[y][x]
            except IndexError:
                pass
            else:
                result.append(point)
        return result

    def recur(y, x, current_weight):
        min_weight = sys.maxint
        next_point = None
        for point in get_available(y, x):
            y, x = point
            if (matrix[y][x] + current_weight) < min_weight:
                next_point = point
                min_weight = matrix[y][x] + current_weight
        if next_point is None:
            return
        path.append(next_point)
        y,x = next_point
        recur(y, x, current_weight + matrix[y][x])
    recur(0, 0, matrix[0][0])
    for y,x in path:
        matrix[y][x] = "#"
    print_matrix(matrix)






def main():
    print_path([[9,4,3],
                [2,1,6],
                [0,9,1]])

if __name__ == "__main__":
    main()
