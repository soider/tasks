# coding: utf-8
"""
Даны 5 целых чисел от 1 до 13. Среди них:
- если одинаковы 5, вывести "Impossible"
- если одинаковы 4, вывести "Four of a Kind"
- если одинаковы 3 и 2, вывести "Full house"
- если есть 5 последовательных, то вывести "Straight"
- если одинаковы 3, то вывести "Three of a Kind
- если одинаковы 2 и 2, то вывести "Two pairs"
- если одинаковы 2, то вывести "One pair"
- иначе Nothing
"""


def poker(cards):
    card_counter = [0] * 14
    combination_counter = [0] * 14
    prev = None
    all_same = True
    for card in cards:
        if prev is None:
            prev = card
        else:
            if prev != card:
                all_same = False
        card_counter[card] += 1
    for counter in card_counter:
        combination_counter[counter] += 1
    if all_same:
        return "Impossible"
    if combination_counter[4]:
        return "Four of a Kind"
    if combination_counter[3] and combination_counter[2]:
        return "Full house"
    if len([c for c in card_counter if c == 2]) == 1:
        return "One pair"
    if len([c for c in card_counter if c == 2]) == 2:
        return "Two pairs"
    increasing = True
    prev = None
    for x in sorted(cards):
        if prev is None:
            prev = x
        else:
            if not (prev + 1 == x):
                increasing=False
            prev = x
    if increasing:
        return "Straight"
    return "Nothing"


if __name__ == "__main__":
    assert poker([1, 1, 1, 1, 1]) == "Impossible"
    assert poker([1, 1, 1, 1, 5]) == "Four of a Kind"
    assert poker([1, 1, 1, 2, 2]) == "Full house"
    assert poker([1, 3, 9, 3, 2]) == "One pair"
    assert poker([1, 5, 5, 4, 4]) == "Two pairs"
    assert poker([1, 5, 2, 4, 3]) == "Straight"
    assert poker([10, 11, 12, 13, 1]) == "Nothing"
