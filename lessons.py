#!/bin/python

import sys


line_count = int(raw_input().strip())
for line in xrange(line_count):
    n,k = raw_input().strip().split(' ')
    students_total,students_minimal_value = int(n), int(k)
    timings = map(int, raw_input().strip().split())
    assert len(timings) == students_total, "Wrong fields count in line"
    have_students = len([value for value in timings if value <= 0])
    if have_students < students_minimal_value:
        print "YES"
    else:
        print "NO"
    
