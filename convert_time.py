#!/bin/python

import sys


time = raw_input().strip()
mode = time[-2:]
time = time[:-2]

assert mode in ("AM", "PM")
hours, minutes, secs = map(int, time.split(":"))
if mode == "AM" or hours == 12:
    print time
else:
    print "%02d:%02d:%02d" % (hours+12, minutes, secs)
