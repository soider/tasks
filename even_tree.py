def print_graph(d):
    for line in d:
        print " ".join(map(str, line))

verticles, edges = map(int, raw_input().split())
graph = []
for _ in range(verticles):
    graph.append([0] * verticles)
print_graph(graph)
for _ in range(edges):
    ver1, ver2 = map(int, raw_input().split())
    graph[ver1-1][ver2-1] = 1

print "=" * 10
print_graph(graph)