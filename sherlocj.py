n, k, q = map(int, raw_input().split())
a = map(int, raw_input().split())
qs = []
new_a = [None] * n
for _ in range(q):
    qs.append(int(raw_input()))

for i in range(n):
    new_i = i + k
    if new_i >= n:
        new_i = new_i % n
    new_a[new_i] = a[i]
    
for q in qs:
    print new_a[q]
