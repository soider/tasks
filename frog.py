def frog(x, y, d):
    i = (y - x) // d
    if (i * d) + x < y:
        i+=1
    return i

if __name__ == "__main__":
    assert frog(10, 85, 30) == 3
    assert frog(1, 5, 2) == 2
