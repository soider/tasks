commands_count = int(raw_input())
accum = []
for _ in range(commands_count):
    line = raw_input().split()
    command = line[0]
    arguments = line[1:]
    if command == "insert":
        position, value = arguments
        accum.insert(int(position), int(value))
    elif command == "print":
        print accum
    elif command == "remove":
        value = arguments[0]
        accum.remove(int(value))
    elif command == "append":
        value = arguments[0]
        accum.append(int(value))
    elif command == "sort":
        accum.sort()
    elif command == "pop":
        accum.pop()
    elif command == "reverse":
        accum = list(reversed(accum))
    else:
        assert False, "Error, unknown command!"