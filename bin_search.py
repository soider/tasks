v = int(raw_input())
raw_input()
data = map(int, raw_input().split())


def binary(v, data):
    right = len(data) - 1
    left = 0

    while True:
        middle = (left + right) / 2
        if data[middle] == v:
            return middle
        elif data[middle] < v:
            left = middle + 1
        elif data[middle] > v:
            right = middle - 1
            

print binary(v, data)
