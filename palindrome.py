def isp(inp):
    left = 0
    right = len(inp) - 1
    while left < right:
        while left != right and not inp[left].isalnum():
            left += 1
        while right != left and not inp[right].isalnum():
            right -= 1
        if inp[left].lower() != inp[right].lower():
            return False
        left += 1
        right -= 1
    return True


assert isp("A man, a plan, a canal: Panama")
assert not isp("car")
assert isp("")
