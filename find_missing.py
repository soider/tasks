class Solution:
    # @param A : list of integers
    # @return an integer
    def firstMissingPositive(self, A):
        N = len(A)
        j = 0 # count of shifted neggatives
        for i in range(N):
            if A[i] <= 0:
                A[i], A[j] = A[j],A[i]
                j += 1
        for i in range(N):
            if abs(A[i]) < N:
                A[abs(A[i])] \
                    = - abs(A[abs(A[i])])
        for i in range(j,N):
            if A[i] > 0:
                return i-j+1
        return N - j + 1

            
        # for i in range(j, N):
        #     if A[i] > 0:
        #         print i
        #         1/0
        #         pass
        #     print A[i]
        # return j + i + 1
        

def find_m(inp):
    return Solution().firstMissingPositive(inp)


assert find_m([1]) == 2
assert find_m([1,2,3,4,0,-7,-6,-5]) == 5
assert find_m([1,2,0]) == 3
assert find_m([3,4,-1,1]) == 2
assert find_m([-8, -7, -6]) == 1
