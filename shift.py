def solution(inp, k):
    if not inp:
        return inp
    
    length = len(inp)
    if k > length:
        k = k % length
    stack = []
    for i in range(k):
        stack.append(inp[length - i - 1])
    for i in range(length):
        new_position = (i + k) % length
        inp[new_position] = inp[i]
    for i in range(k):
        try:
            inp[i] = stack.pop()
        except IndexError:
            pass
    return inp

def solution1(inp, k):
    result = [0] * len(inp)
    for i in range(len(inp)):
        new_position = (i + k) % len(inp)
        result[new_position] = inp[i]
    return result
    

assert solution([1],2) == [1]
assert solution([1],1) == [1]
assert solution([1,2], 5) == [2,1]
assert solution([], 1) == []
assert solution([3,8,9,7,6], 3) == [9,7,6,3,8]
