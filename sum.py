def find_sum(target, data):
    # sorry, couldn't find corner case :)
    dict_counter = {}
    for idx, num in enumerate(data):
        dict_counter[idx] = num
        if (target - num) in dict_counter:
            return True
    return False


if __name__ == "__main__":
    target = int(raw_input())
    count = int(raw_input())
    array = []
    for _ in range(count):
        array.append(int(raw_input()))
    if find_sum(target, array):
        print 1
    else:
        print 0
