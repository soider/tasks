MARKER = -128


def find_delta(data):
    result = [data[0]]
    for idx, el in enumerate(data[1:]):
        val = el - data[idx]
        if not(-127 <= val <= 127):
            result.append(MARKER)
        result.append(val)
    return result

if __name__ == "__main__":
    data = map(int, raw_input().split())
    print " ".join(map(str, find_delta(data)))
