matrix1 = [
    [335, 401, 128, 384, 345, 275, 324, 139, 127, 343, 197, 177, 127, 72, 13, 59],
    [102, 75, 151, 22, 291, 249, 380, 151, 85, 217, 246, 241, 204, 197, 227, 96],
    [261, 163, 109, 372, 238, 98, 273, 20, 233, 138, 40, 246, 163, 191, 109, 237],
    [179, 213, 214, 9, 309, 210, 319, 68, 400, 198, 323, 135, 14, 141, 15, 168]
]

LEFT = 0
RIGHT = 1
TOP = 2
BOTTOM = 3


def spiral(m):
    result = []
    top = 0
    left = 0
    bottom = len(m) - 1
    right = len(m[0]) - 1 
    direction = RIGHT
    while top <= bottom and left <= right:
        if direction == RIGHT:
            for x in range(left, right + 1):
                result.append(m[top][x])
            top += 1
            direction = BOTTOM
            continue
        if direction == BOTTOM:
            for x in range(top, bottom + 1):
                result.append(m[x][right])
            right -= 1
            direction = LEFT
            continue
        if direction == LEFT:
            for x in range(right, left-1, -1):
                result.append(m[bottom][x])
            direction = TOP
            bottom -= 1
            continue
        if direction == TOP:
            for x in range(bottom, top+1):
                result.append(m[x][left])
            direction = RIGHT
            left += 1
    
    print result
    return result


expect = [335, 401, 128, 384, 345, 275, 324, 139, 127, 343, 197,
          177, 127, 72, 13, 59, 96, 237, 168, 15, 141, 14, 135,
          323, 198, 400, 68, 319, 210, 309, 9, 214, 213, 179,
          261, 102, 75, 151, 22, 291, 249, 380, 151, 85, 217,
          246, 241, 204, 197, 227, 109, 191, 163, 246, 40, 138,
          233, 20, 273, 98, 238, 372, 109, 163]
print expect
assert spiral(matrix1) == expect
