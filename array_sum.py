def sum(a, b):
    a = list(reversed(a))
    b = list(reversed(b))
    result = []
    overflow = 0
    base = 10
    for left, right in map(None, a, b):
        if left is None:
            left = 0
        if right is None:
            right = 0
        r = left + right + overflow
        if r >= base:
            overflow = (r / base)
            r = r % base
        else:
            overflow = 0
        result.append(r)
    result.append(overflow)
    r = 0
    for idx, p in enumerate(result):
        r += p * (10**idx)
    return r


assert sum([9, 8], [3, 1]) == 129
assert sum([9, 9, 9], [1]) == 999 + 1
assert sum([1, 0], [1]) == 10 + 1
assert sum([1, 0, 0], [1, 0]) == 100 + 10
