def fib(n):
    nums = [0, 1]
    for x in range(2, n+1):
        nums.append(nums[x-1] + nums[x-2])
    return nums[-1]

def fib_m(a, b, n):
    nums = [0, 1]
    for x in range(2, n):
        nums.append(nums[-2] + nums[-1] * nums[-1])
    print nums[-1]
    

assert fib(7) == 13
assert fib(9) == 34


fib_m(5)
