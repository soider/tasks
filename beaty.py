def beaty(n):
    count = 0
    n = list(n)
    pattern = ['0', '1', '0']
    length = len(pattern)
    for x in range(0, len(n)):
        window = n[x:x+length]
        if window == pattern:
            count += 1
            try:
                if n[x+3] == '1':
                    n[x+2] = '1'
                else:
                    n[x+1] = '0'
            except IndexError:
                n[x+1] = '0'
    return count

assert beaty("0101010") == 2
assert beaty("01100") == 0
assert beaty("0100101010") == 3
assert beaty("") == 0
assert beaty("010") == 1
assert beaty("0101") == 1
