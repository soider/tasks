
def naive_clock(a, b, c, d):
    try:
        result_parts = [None, None, None, None]
        parts = [a, b, c, d]
        result_parts[0] = max(e for e in parts if e <= 2)
        parts.remove(result_parts[0])
        if result_parts[0] == 2:
            second_num_max = 3
        else:
            second_num_max = 9
        result_parts[1] = max(e for e in parts if e <= second_num_max)
        parts.remove(result_parts[1])
        result_parts[2] = max(e for e in parts if e <= 5)
        parts.remove(result_parts[2])
        result_parts[3] = max(e for e in parts if e <= 9)
        res = "%d%d:%d%d"  % tuple(result_parts)
        return res
    except ValueError:
        return IMPOSIBLE

IMPOSIBLE = "NOT POSSIBLE"

def clock(a, b, c, d):
    try:
        limits = [2, 9, 5, 9]
        result = [None, None, None, None]
        parts = [a, b, c, d]
        for idx, limit in enumerate(limits):
            if idx == 1 and result[0] == 2:
                limit = 3
            current_value = max(e for e in parts if e <= limit)
            parts.remove(current_value) # not optimal but we always have max 4 elements in list, so even n^2 is fine here
            result[idx] = current_value
        res =  "%d%d:%d%d" % tuple(result)
        return res
    except ValueError:
        return IMPOSIBLE


assert clock(2,4,0,0) == "20:40"
assert clock(3,0,7,0) == "07:30"
assert clock(9, 1, 9, 7) == IMPOSIBLE
assert clock(9, 7, 8, 3) == IMPOSIBLE

def randtest():
    import random
    while True:
        parts = [random.randint(0, 9) for _ in range(4)]
        assert naive_clock(*parts) == clock(*parts)
randtest()