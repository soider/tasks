def solution(password):
    max_length = -1
    current_part_is_valid = False
    cur_len = 0
    for symbol in password:
        if not symbol.isalpha():
            if cur_len > max_length and current_part_is_valid:
                max_length = cur_len
            current_part_is_valid = False
            cur_len = 0
            continue
        if symbol.isupper():
            current_part_is_valid = True
        cur_len += 1
    if cur_len > max_length and current_part_is_valid:
        max_length = cur_len
    return max_length


assert solution("a0bb") == -1
assert solution("a0Ba") == 2
assert solution("a0Ba0Bbba") == 4
assert solution("") == -1
assert solution("a0Bbba0Ba") == 4
assert solution("asdfasdwweqA0") == 12
assert solution("A0asdkjasdkhbasd") == 1



