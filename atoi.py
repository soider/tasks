def atoi(string):
    result = 0
    power_num = 0
    x = len(string)
    for i in range(x-1, -1, -1):
        try:
            result += int(string[i]) * (10 ** power_num)
            power_num += 1
        except Exception:
            return -1
    return result


assert atoi("12345") ==  12345
assert atoi("543") ==  543
assert atoi("01231") ==  1231
assert atoi("0123X") ==  -1