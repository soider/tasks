def dirReduc(a):
    changed = True
    def is_pair(first, second):
        y = ['NORTH', 'SOUTH']
        x = ['EAST', 'WEST']
        if first != second:
            if first in x and second in x:
                return True
            if first in y and second in y:
                return True
        return False
    while changed:
        changed = False
        final_idx = len(a) - 1
        for idx, element in enumerate(a[:]):
            if idx != final_idx:
                next_element = a[idx+1]
                if is_pair(element, next_element):
                    del a[idx+1]
                    del a[idx]
                    changed = True
                    break
    return a

if __name__ == "__main__":
    a = ["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]
    assert dirReduc(a) == ['WEST']
    u=["NORTH", "WEST", "SOUTH", "EAST"]
    assert dirReduc(u) == ["NORTH", "WEST", "SOUTH", "EAST"]
