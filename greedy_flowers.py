import itertools
import collections

def game(k, flowers):
    total_amount = 0
    flowers.sort(reverse=True)
    orders = collections.defaultdict(list)
    for buyer in itertools.cycle(range(k)):
        if not flowers:
            break
        flower = flowers.pop(0)
        total_amount = total_amount + flower * (len(orders[buyer]) + 1)
        orders[buyer].append(flower)
    return total_amount


assert game(3, [2,5,6]) == 13
assert game(2, [2,5,6]) == 15