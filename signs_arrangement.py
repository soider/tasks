# coding: utf-8
"""Даны N целых чисел. Расставить между ними знаки "+" или "-" так, чтобы
значение получившегося выражения равнялось заданому целому S"""
import sys


def arrange(data, s):
    first = data[0]
    n = len(data)
    signs = [None] * (n-1)
    def bruteforce(k, accum):
        """Обход с конца массива `data`"""
        if k == 1:
            if accum + first == s:
                print_answer(signs, data)
                sys.exit(0)
        else:
            signs[k-2] = '+'
            bruteforce(k-1, accum + data[k-1])
            signs[k-2] = '-'
            bruteforce(k-1, accum - data[k-1])
    bruteforce(n, 0)
    print "No answer found"
    sys.exit(1)


def print_answer(signs, data):
    for idx, element in enumerate(data):
        if idx != len(data) - 1:
            print element,
            print signs[idx],
    print data[-1]


if __name__ == "__main__":
    print arrange([15, 25, 30], 10)
