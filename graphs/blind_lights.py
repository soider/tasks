import sys
import collections

class Graph(object):
    
    def __init__(self, nodes):
        self.nodes_count = nodes
        self.weights = collections.defaultdict(dict)
        for n in range(nodes):
            self.weights[n][n] = 0
        
    def add_edge(self, node_from, node_to, weight):
        self.weights[node_from][node_to] = weight
        
    def compute(self):
        for k in range(self.nodes_count):
            for i in range(self.nodes_count):
                for j in range(self.nodes_count):
                    if self.weights[i].get(j, sys.maxint) > self.weights[i].get(k, sys.maxint) + self.weights[k].get(j, sys.maxint):
                        self.weights[i][j] = self.weights[i][k] + self.weights[k][j]

    def find_way(self, node_from, node_to):
        if node_to in self.weights[node_from]:
            return self.weights[node_from][node_to]
        return -1
        #     max_k = self.nodes_count - 1
        # if max_k == 0: # base case
        #     return self.edge_weights[(node_from, node_to)]
        # return self.find_way(node_from, node_to, max_k - 1)

def main():
    nodes, edges = map(int, raw_input().split())
    graph = Graph(nodes)
    for _ in range(edges):
        node_from, node_to, weight = map(int, raw_input().split())
        node_from -= 1
        node_to -= 1
        graph.add_edge(node_from, node_to, weight)
    questions_count = int(raw_input())
    # graph.compute()
    for _ in range(questions_count):
        a, b = map(int, raw_input().split())
        a -= 1
        b -= 1
        print graph.find_way(a, b)

if __name__ == "__main__":
    main()