def read_oriented_graph_list(nodes, edges):
    graph = []
    for _ in range(nodes+1):
        graph.append([])
    graph[0] = None
    for _ in range(edges):
        ver1, ver2 = map(int, raw_input().split())
        graph[ver2].append(ver1)
        graph[ver1].append(ver2)
    return graph

DISCOVERED = 1
PROCESSED = 2

def answer(graph, start_from):
    queue = [start_from]
    distances = [None] * len(graph)
    states = {}
    discovered = set()
    time = 0
    distances[start_from] = 0
    while queue:
        time += 1
        node = queue.pop(0)
        for vertex_to in graph[node]:
            if vertex_to not in discovered:
                if distances[vertex_to] is not None: # we have some path to this node
                    distances[vertex_to] = min(distances[vertex_to], distances[node] + 6)
                else:
                    distances[vertex_to] = distances[node] + 6
                queue.append(vertex_to)
                discovered.add(vertex_to)
                continue
    for node in range(len(graph)):
        if node == 0:
            continue
        if node not in discovered:
            distances[node] = -1
    res = []
    for node in range(len(distances)):
        if node == 0:
            continue
        if node == start_from:
            continue
        res.append(str(distances[node]))
    return res
    
    

if __name__=="__main__":
    q = int(raw_input())
    for _ in range(q):
        nodes, edges = map(int, raw_input().split())
        graph = read_oriented_graph_list(nodes, edges)
        start_from = int(raw_input())
        print " ".join(answer(graph, start_from))