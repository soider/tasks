def print_graph_matrix(d):
    for line in d:
        print " ".join(map(str, line))


def read_oriented_graph_matrix():
    verticles, edges = map(int, raw_input().split())
    graph = []
    for _ in range(verticles):
        graph.append([0] * verticles)
    for _ in range(edges):
        ver1, ver2 = map(int, raw_input().split())
        graph[ver1-1][ver2-1] = 1
    return graph
    

def read_oriented_graph_list():
    verticles, edges = map(int, raw_input().split())
    graph = []
    for _ in range(verticles+1):
        graph.append([])
    graph[0] = None
    for _ in range(edges):
        ver1, ver2 = map(int, raw_input().split())
        graph[ver2].append(ver1)
    return graph

def print_vert(x):
    print "Node %s" % x

def print_edge(x, y):
    print "%s -> %s" % (x, y)

def bfs(graph, root=1, process_before=None, process_after=None, process_edge=None):
    queue = [root]
    discovered = [False] * len(graph)
    processed = [False] * len(graph)
    parent = [False] * len(graph)
    while queue:
        vert = queue.pop(0)
        processed[vert] = True
        if process_before is not None:
            process_before(vert)
        for p in graph[vert]:
            if process_edge is not None:
                process_edge(vert, p)
            if not discovered[p]:
                queue.append(p)
                discovered[p] = True
        if process_after is not None:
            process_after(vert)
        
def dfs(graph, root=1, process_before=None, process_after=None, process_edge=None):
    print graph
    queue = [root]
    discovered = [False] * len(graph)
    processed = [False] * len(graph)
    parent = [False] * len(graph)
    while queue:
        print queue
        vert = queue.pop()
        if processed[vert]:
            continue
        print vert
        processed[vert] = True
        if process_before is not None:
            process_before(vert)
        for p in graph[vert]:
            print "*", p 
            if process_edge is not None:
                process_edge(vert, p)
            if not discovered[p]:
                queue.append(p)
                discovered[p] = True
        if process_after is not None:
            process_after(vert)



def find_components(graph):
    from collections import defaultdict
    c = 0
    components = defaultdict(set)
    discovered = [False] * len(graph)
    def process_before(vert):
        discovered[vert] = True
    def process_edge(x, y):
        components[c].add(x)
        components[c].add(y)
    for x in range(1, len(graph)):
        if discovered[x] == False:
            c += 1
            bfs(graph, root=x, process_before=process_before, process_edge=process_edge)
    # print discovered
    return dict(components.items())
    

def find_edges(graph):
    edges = []
    def process_edge(x, y):
        edges.append((x,y))
    bfs(graph, root=1, process_edge=process_edge)
    return edges


def delete_edge(graph, x, y):
    graph[x].remove(y)


def add_edge(graph, x, y):
    graph[x].append(y)


def even_tree(graph):
    verts_count = len(graph) - 1
    edges = find_edges(graph)
    counter = 0
    for edge in edges:
        # print "Checking edge %s -> %s" % edge
        # print graph
        x, y = edge
        delete_edge(graph, x, y)
        components = find_components(graph)
        all_even = True
        total_components_len = 0
        for component in components.values():
            if len(component) % 2 != 0:
                all_even = False
                break
            total_components_len += len(component)
        # print "Total comp", total_components_len, "Grapg len", verts_count
        if not all_even:
            # print "Returning edge %s -> %s" % edge
            add_edge(graph, x, y)
        elif total_components_len != verts_count:
            # print "Missed somethinh", total_components_len, verts_count
            add_edge(graph, x, y)    
        else:
            # print "Deleting edge %s -> %s" % edge
            counter += 1
    print counter
        

if __name__ == "__main__":
    # print_graph(read_oriented_graph_matrix())
    g = read_oriented_graph_list()
    # print g
    dfs(g)
    # even_tree(g)
    # print g