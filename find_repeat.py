def findr(ar):
    data = {}
    for x in ar:
        try:
            data[x] += 1
        except KeyError:
            data[x] = 0
        else:
            return x
    return -1

case = [3, 4, 1, 4, 1]

print findr(case)
assert findr(case) == 1
