def fact(n):
    if n == 1:
        return 1
    if n == 2:
        return 2
    b = 2
    for i in xrange(n+1):
        if i > 2:
            b = b * i
    return b

assert fact(1) == 1
assert fact(2) == 2
assert fact(3) == 6
assert fact(4) == 24
assert fact(10) == 3628800
assert fact(25) == 15511210043330985984000000
