def viral(x):
    start = 5
    r = 0
    for _ in range(x):
        r += start/2
        start = start/2 * 3
    return r


assert viral(3) == 9
assert viral(4) == 15