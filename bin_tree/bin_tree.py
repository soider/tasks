class TreeNode(object):
    def __init__(self, value, left=None, right=None):
        self.value = value
        self.left = left
        self.right = right
        self.parent = None

    def add(self, node):
        node.parent = self
        if node.value < self.value:
            if self.left is None:
                self.left = node
                return
            self.left.add(node)
        elif node.value > self.value:
            if self.right is None:
                self.right = node
                return
            self.right.add(node)
        else:
            assert False, "Not implemented for same values"
    
    def find(self, value):
        if self.value == value:
            return self
        if self.value > value:
            if self.left is None:
                return None
            return self.left.find(value)
        if self.value < value: 
            if self.right is None:
                return None
            return self.right.find(value)

    def minimum(self):
        if self.left is None:
            return self
        return self.left.minimum()

    def maximum(self):
        if self.right is None:
            return self
        return self.right.maximum()

    def next(self):
        if self.right is not None:
            return self.right.minimum()
        p = self.parent
        x = self
        while p is not None and p.right == x:
            x = p
            p = p.parent
        return p

    def prev(self):
        if self.left is not None:
            return self.left.maximum()
        p = self.parent
        x = self
        while p is not None and p.left == x:
            x = p
            p = p.parent
        return p

    def print_node(self, level, app=None):
        marker = "-" * level
        if app is not None:
            marker = marker + app
        print marker, self.value
        if self.left is not None:
            self.left.print_node(level + 1, app="L: ")
        if self.right is not None:
            self.right.print_node(level + 1, app="R: ")

    def __repr__(self):
        return "Node: %s, [left %s, right %s] " % (self.value, self.left, self.right)

class BinaryTree(object):
    
    def __init__(self):
        self.root = None
        self.count = 0

# public static boolean isBST(TreeNode node, int leftData, int rightData)
# {
#     if (node == null)
#         return true;
    
#     if (node.getData() > leftData || node.getData() <= rightData)
#         return false;
    
#     return (isBST(node.left, node.getData(), rightData) && isBST(node.right, leftData, node.getData()));
# }
    def check_invariant(self, root):
        
        if root is None:
            return
        if root.left is not None:
            assert root.value > root.left.value
        if root.right is not None:
            assert root.value < root.right.value
        self.check_invariant(root.left)
        self.check_invariant(root.right) 

    def add(self, value):
        self.count += 1
        if self.root is None: # empty tree
            self.root = TreeNode(value)
        else:
            self.root.add(TreeNode(value))

    def find(self, value):
        return self.root.find(value) 

    def next(self, value):
        node = self.find(value)
        if node is None:
            return None
        return node.next() 

    def prev(self, value):
        node = self.find(value)
        if node is None:
            return None
        return node.prev() 

    def remove(self, value):
        node = self.find(value)
        if node is None:
            return
        parent = node.parent
        if node.right is None:
            if parent.left is node:
                parent.left = node.left
            if parent.right is node:
                parent.right = node.left
        else:
            next = node.next()
            assert next.left is None, "Found next with non null left child"
            node.value = next.value
            if next.parent.right is next:
                next.parent.right = None
            if next.parent.left is next:
                next.parent.left = None
            

    def __repr__(self):
        return "[Tree: %s]" % self.root
    
    def print_tree(self):
        self.root.print_node(1)


if __name__ == "__main__":
    tree = BinaryTree()
    tree.add(1)
    tree.add(5)
    tree.add(6)
    tree.add(3)        
    tree.add(4)
    tree.add(7)
    tree.add(9)
    tree.add(2)
    tree.add(-1)
    tree.add(-5)
    tree.add(-8)
    tree.add(-3)
    tree.add(-4)
    
    tree.check_invariant(tree.root)
    tree.print_tree()
    
    assert tree.find(5).value == 5
    assert tree.find(5).next().value == 6
    assert tree.find(6).next().value == 7
    assert tree.find(4).prev().value == 3
    assert tree.find(1).next().value == 2
    assert tree.find(5).next().value == 6
    assert tree.find(3).next().value == 4
    assert tree.find(2).next().value == 3
    assert tree.find(4).next().value == 5
    assert tree.find(6).next().value == 7
    assert tree.find(7).next().value == 9
    assert tree.find(9).next() is None
    assert tree.find(1).prev().value == -1 
    assert tree.find(5).prev().value == 4
    assert tree.find(3).prev().value == 2
    assert tree.find(2).prev().value == 1
    assert tree.find(4).prev().value == 3
    assert tree.find(6).prev().value == 5
    assert tree.find(7).prev().value == 6
    assert tree.find(-3).next().value == -1

    tree.remove(-5)
    tree.check_invariant(tree.root)
