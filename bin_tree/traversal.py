#python2
import sys
sys.setrecursionlimit(10**6) # max depth of recursion
# threading.stack_size(2**27)  # new thread will get stack of such size


class Tree(object):
    
    def __init__(self, size):
        self.size = size
        self.left = [-1] * size
        self.right = [-1] * size
        self.values = [None] * size

    def set_value(self, num, value):
        self.values[num] = value

    def set_left(self, num, target):
        self.left[num] = target

    def set_right(self, num, target):
        self.right[num] = target

    def traversal(self):
        return [
            self.in_order(0),
            self.pre_order(0),
            self.post_order(0),
        ]

    def print_stack(self, stack):
        print list(map(lambda e: self.values[e], stack))

    def post_order(self, _):
        result = []
        stack = []
        current = 0
        while True:
            self.print_stack(stack)
            while current != -1:
                if self.right[current] != -1:
                    stack.append(self.right[current])
                stack.append(current)
                current = self.left[current]
            current = stack.pop()
            if stack and self.right[current] != -1 and\
            stack[-1] == self.right[current]:
                stack.pop()
                stack.append(current)
                current = self.right[current]
            else:
                result.append(self.values[current])
                current = -1
            if not stack:
                break
        return result

    def in_order(self, _):
        result = []
        stack = []
        current = 0
        while True:
            if current != -1:
                stack.append(current)
                current = self.left[current]
            else:
                if stack:
                    current = stack.pop()
                    result.append(self.values[current])
                    current = self.right[current]
                else:
                    break
        return result
    
    def pre_order(self, _):
        result = []
        stack = [0]
        while stack:
            node = stack.pop()
            if self.right[node] != -1:
                stack.append(self.right[node])
            if self.left[node] != -1:
                stack.append(self.left[node])
            result.append(self.values[node])
        return result

  
   

def main(size, edges):
    t = Tree(size)
    for num, (value, left, right) in enumerate(edges):
        t.set_value(num, value)
        t.set_left(num, left)
        t.set_right(num, right)
    return t.traversal()

if __name__ == "__main__":
    res = main(5, [(4, 1, 2), (2, 3, 4), (5, -1, -1), (1, -1, -1), (3, -1,- 1)])
    # print res
    # assert res == [[1,2,3,4,5], [4,2,1,3,5], [1,3,2,5,4]]

    size = int(raw_input())
    edges = [map(int, raw_input().split()) for _ in range(size)]
    res = main(size, edges)
    for r in res:
        print " ".join(map(str, r))
