class Node(object):
    
    def __init__(self, value):
        self.left = None
        self.right = None
        self.parent = None
        self.value = value

    def insert(self, node):
        node.parent = self
        assert node.value != self.value, "Doesn't support simillar values'"
        if self.value > node.value:
            if self.left is None:
                self.left = node
            else:
                self.left.insert(node)
        else:
            if self.right is None:
                self.right = node
            else:
                self.right.insert(node)

    def next(self):
        if self.right is not None:
            return self.right.find_minimum()
        p = self.parent
        x = self
        while p is not None and p.left != x:
            print p, x, x == self
            x = p
            p = x.parent
        return p
            
    def _delete_one_child(self, node):
        if self.right is node:
            self.right = None
        if self.left is node:
            self.left = None

    def find(self, value):
        if self.value == value:
            return self
        try:
            if self.value > value:
                return self.left.find(value)
            else:
                return self.right.find(value)
        except AttributeError:
            return self # return nearest node
    def find_minimum(self):
        if self.left is None:
            return self
        return self.left.find_minimum()

    def remove(self, value):
        node = self.find(value)
        if node.value != value:
            return
        if node.left is None and node.right is None:
            node.parent._delete_one_child(node)
            return
        next = node.next()
        next_value = next.value
        if next.right is None: # right subtree is empty, just swap values and delete next
            node.value = next.value
            next.parent._delete_one_child(next)
        else:
            next.value = next.right.value
            assert next.left is None, "Have left subtree in next while deleting is not None"
            next.right = next.right.right
            next.left = next.left.left
            
    def print_node(self, level=1, app=None):
        if level == 1:
            print "=" * 10
        marker = "-" * level
        if app is not None:
            marker = marker + app
        print marker, self.value
        if self.left is not None:
            self.left.print_node(level + 1, app="L: ")
        if self.right is not None:
            self.right.print_node(level + 1, app="R: ")
    
    @property
    def root(self):
        parent = self.parent
        x = self
        while parent != None:
            x = parent
            parent = parent.parent
        return x

    def right_rotate(self):
        # assert self.parent is not None, "Can't rotate root'"
        # assert self.parent.left is self
        old_root = self
        par_node = old_root.parent
        
        new_root = old_root.left
        temp = new_root.left
        old_root.left = new_root.right
        if old_root.left:
            old.root.left.parent = old_root
        new_root.right = old_root
        old_root.parent = new_root
        if par_node is None: # rotate over root
            pass 
        else:
            if par_node.right and par_node.right.value == old_root.value:
                par_node.right = new_root
                new_root.parent = par_node
            elif par_node.left and par_node.left.value == old_root.value:
                par_node.left = new_root
                new_root.parent = par_node
            
        # old_parent = self.parent
        # old_right = self.right

        # self.parent.left = None
        # self.parent = None
        # self.right = None
        # self.right = old_parent
        # old_parent.parent = self
        # # if old_right is not None:
        # old_right.parent = old_parent
        # old_parent.left = old_right
   

    def left_rotate(self):
        
        assert self.parent is not None, "Can't rotate root'"
        assert self.parent.right is self

        old_parent = self.parent
        old_left = self.left

        self.parent = None
        self.left = old_parent
        old_parent.parent = self

        old_parent.right = old_left
        old_left.parent = old_parent 
        

    def __repr__(self):
        return "NODE: %s [L: %s| R: %s]" % (self.value, self.left, self.right)


if __name__ == "__main__":
    tree = Node(3)
    tree.insert(Node(2))
    tree.insert(Node(2.5))
    tree.insert(Node(1))
    tree.insert(Node(5))
    tree.insert(Node(4))
    tree.insert(Node(6))
    tree.root.print_node()
    tree.find(2).right_rotate()
    tree.root.print_node()
    # tree.find(3).left_rotate()
    # tree.root.print_node()