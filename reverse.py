def rev(n):
    result = []
    length = len(n)
    for i in  range(length):
        result.append(n[length - i - 1])
    return result

def rev(n):
    result = []
    x = len(n) / 2
    for i in range(x):
        n[i],n[len(n) -i -1] = n[len(n) -i -1], n[i]
    return n


assert rev([1,2,3,4]) == [4,3,2,1]
assert rev([1,2,3]) == [3,2,1]
assert rev([1,2,3,4,5]) == [5,4,3,2,1]
