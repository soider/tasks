# Definition for an interval.

def is_overlaps(x, y):
    if x == y:
        return True
    if x[0] <= y[0]:
        if x[1] >= y[0] or x[1] >= y[1]:
            return True

    elif x[1] <= y[1]:
        return True
    return False


def merge_interval(left, right):
    v = [left[0], right[0], left[1], right[1]]
    l = min(v)
    r = max(v)
    return [l, r]

def merge(intervals, new_interval):
    # merge current
    merged = False
    result = []
    for idx, interval in enumerate(intervals):
        if is_overlaps(interval, new_interval) and not merged:
            intervals[idx] = merge_interval(interval, new_interval)
            merged = True
    # post
    merged = False
    for idx, interval in enumerate(intervals):
        if merged:
            continue
        if not result:
            result.append(interval)
            continue
        if is_overlaps(result[-1], interval):
            result[-1] = merge_interval(result[-1], interval)
        else:
            result.append(interval)
    return result
        


assert is_overlaps([1,3], [1,3])
assert is_overlaps([1,3], [2,5])
assert is_overlaps([1,3], [6,9]) == False
assert is_overlaps([1, 7], [2,5])
assert is_overlaps([1,2], [3,5]) == False
assert is_overlaps([1,2], [4,9]) == False

assert merge([[1,2],[3,5],[6,7],[8,10],[12,16]], [4, 9]) == [[1,2],[3,10], [12, 16]]
assert merge([[1,3],[6,9]], [2,5]) == [[1,5], [6,9]]
assert merge([ (1, 2), (3, 6) ], (10,8) ) == [(1, 2), (3, 6), (8, 10)]
