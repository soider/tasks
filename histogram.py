def largest_histogram_naive(inp):
    
    def find_left(inp, idx, value):
        count = 1
        for i in range(idx-1, -1, -1):
            if inp[i] < value:
                break
            count += 1
        return count


    def find_right(inp, idx, value):
        count = 0
        for i in range(idx+1, len(inp)):
            if inp[i] < value:
                break
            count += 1
        return count

    max_square = inp[0]
    if len(inp) == 1:
        return max_square
    for idx, element in enumerate(inp):
        left = find_left(inp, idx, element)
        right = find_right(inp, idx, element)
        els = left+right
        if els * element > max_square:
            max_square = els * element
    return max_square


def largest_histogram(inp):
    pass



assert largest_histogram([10, 40, 30, 70, 10, 30, 60]) == 90
# assert largest_histogram([5]) == 5
# assert largest_histogram([5, 3]) == 6
# assert largest_histogram([1, 1, 4, 1]) == 4
# assert largest_histogram([1, 1, 3, 1]) == 4
# assert largest_histogram([2, 1, 4, 5, 1, 3, 3]) == 8
