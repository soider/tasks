def Xbonacci(signature,n):
    size = len(signature)
    current_sum = 0
    for x in range(size, n):
        if current_sum == 0:
            current_sum = sum(signature[:size])
        s = sum(signature[-size:])
        if s:
            signature.append(s)
    return signature[:n]


if __name__ == "__main__":
    assert Xbonacci([0,1],10) == [0,1,1,2,3,5,8,13,21,34]
    assert Xbonacci([1,1],10) == [1,1,2,3,5,8,13,21,34,55]
    assert Xbonacci([0,0,0,0,1],10) == [0,0,0,0,1,1,2,4,8,16]
    assert Xbonacci([1,0,0,0,0,0,1],10) == [1,0,0,0,0,0,1,2,3,6]
    print Xbonacci([0,0,0,0,1],1)
