def solution(n):
    max_gap_length = -1
    current_gap_length = 0
    n = bin(n)[2:]
    for el in n:
        if el == '0':
            current_gap_length += 1
        else:
            max_gap_length = max(max_gap_length, current_gap_length)
            current_gap_length = 0
    max_gap_length = max(max_gap_length, current_gap_length)
    return max_gap_length


assert solution(0) == 1
assert solution(9) == 2
assert solution(529) == 4
assert solution(1041) == 5
