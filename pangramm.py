import collections

def pangram(inp):
    counter = collections.defaultdict(int)
    for ch in inp:
        if ch.isspace():
            continue
        if ch.lower().isalpha():
            counter[ch.lower()] += 1
        else:
            return "not pangram"
    if all([x > 0 for x in counter.values()]) and len(counter) == 26:
        return "pangram"
    return "not pangram"

assert pangram("We promptly judged antique ivory buckles for the next prize") == "pangram"
assert pangram("We promptly judged antique ivory buckles for the prize") == "not pangram"
assert pangram("NOVmETKPTbYu ftZPEykhjgF GGkdGjWGwW skjPJsea dtwdqcr DERCUgxOxrRgDQbdzL IZjyXs") == "pangram"
