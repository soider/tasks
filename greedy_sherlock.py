import sys


def get_variants_for(y):
    variants = [-1]
    for x in range(y, 0, -1):
        if x % 3 == 0 and (y - x) % 5 == 0:
            variants.append(int(str(''.join(['5']*x + ['3'] * (y-x)))))
        elif x % 5 == 0 and (y - x) % 3 == 0:
            variants.append(int(str(''.join(['3']*x + ['5'] * (y-x)))))
    return variants

count = int(raw_input())

for _ in xrange(count):
    inp = int(raw_input())    
    print max(get_variants_for(inp))
