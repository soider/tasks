def tickets(peoples):
    amount = 0
    COST = 25
    for incoming_value in peoples:
        if (incoming_value - COST) > amount:
            return False
        elif incoming_value < COST:
            return False
        amount = amount + incoming_value - (incoming_value - COST)
    return True



if __name__ == "__main__":
    assert tickets([25, 25, 50]) == True
    assert tickets([25, 100]) == False
    assert tickets([24]) == False
    assert tickets([25, 25, 25, 25, 25, 25, 25, 50, 50, 50, 100, 100, 100, 100]) == False
