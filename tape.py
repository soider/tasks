def tape(array):
    result = []
    left_sum = 0
    right_sum = 0
    diff = 0
    for idx in range(len(array)):
        if idx == 0:
            right_part = array[1:]
            left_sum = array[0]
            right_sum = sum(right_part)
            diff = abs(left_sum - right_sum)
        else:
            right_sum -= array[idx]
            left_sum += array[idx]
            tmp_diff = abs(left_sum - right_sum)
            if tmp_diff < diff:
                diff = tmp_diff
    return diff

if __name__ == "__main__":
    assert tape([3, 1, 2, 4, 3]) == 1
