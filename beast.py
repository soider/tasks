def game(n):
    a = n
    while a % 3 != 0:
        a -= 5
    if a < 0:
        return -1
    return int(a*'5'+(n - a)*'3')

# assert game(1) == -1
# assert game(3) == 555
# assert game(5) == 33333
assert game(11) == 55555533333

# 11 = 5 + 3 + 3


# 11 -3 = 8
# 8 -3 = 5
# 5 -3 = 2
# XXXXXXXXXXXXXXX = 555555555555555
# XXXXXX = 
# game(12)