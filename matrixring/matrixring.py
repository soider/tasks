# coding: utf-8
import itertools
import copy


class Matrix(object):

    def __init__(self, *args):
        assert len(set(map(len, args))) == 1, "Invalid matrix dimension"
        self.data = list(args)
        self.new_data = []

        self.dimension = len(self.data[0])
        for idx in range(self.dimension):
            self.new_data.append(self.data[idx][:])

    def rotate(self):
        for x in range(1, self.dimension / 2 + 1):
            self.swap_circle(x)

    def swap_circle(self, circle_number):
        assert circle_number <= self.dimension / 2

        start_row_idx = circle_number - 1
        start_element_idx = circle_number - 1

        end_element_idx = self.dimension - circle_number
        end_row_idx = self.dimension - circle_number

        start_col_idx = circle_number - 1
        end_col_idx = self.dimension - circle_number

        self.shift_row(start_row_idx, start_element_idx, end_element_idx, 1)
        self.shift_row(end_row_idx, start_element_idx, end_element_idx, -1)
        self.shift_col(start_col_idx, start_row_idx, end_row_idx, 1)
        self.shift_col(end_col_idx, start_row_idx, end_row_idx, -1)

        self.data = copy.deepcopy(self.new_data)

    def shift_row(self, row_idx, el_from, el_to, direction):
        print "Will shift %s row from %s to %s" % (row_idx, el_from, el_to)
        row = self.data[row_idx]
        for idx in range(el_from, el_to + 1):
            if direction == 1:
                if idx != el_to:
                    self.new_data[row_idx][idx + 1] = row[idx]
            else:
                if idx != el_from:
                    self.new_data[row_idx][idx - 1] = row[idx]

    def shift_col(self, col_idx, el_from, el_to, direction):
        print "Will shift %s col from %s to %s" % (col_idx, el_from, el_to)
        for current_row_idx in range(el_from, el_to + 1):
            if direction == 1:  # up
                if current_row_idx != el_from:
                    self.new_data[
                        current_row_idx - 1][col_idx] = self.data[current_row_idx][col_idx]
            else:  # down
                if current_row_idx != el_to:
                    self.new_data[
                        current_row_idx + 1][col_idx] = self.data[current_row_idx][col_idx]

    def __eq__(self, other):
        print self, other
        return self.data == other.data

    def render_data(self):
        template = []
        for _ in range(len(self.data)):
            row = []
            for _ in range(len(self.data[0])):
                row.append("%s")
            template.append("\n[%s]" % " ".join(row))
            row = []
        template.append("\n%s" % ('-' * 2 * len(self.data[0])))
        tpl = "".join(template)
        return tpl % tuple(itertools.chain.from_iterable(self.data))

    def __repr__(self):
        return "Matrix(%(n)sx%(n)s):%(data)s" % {'n': self.dimension,
                                                 'data': self.render_data()}
