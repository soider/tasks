from matrixring import Matrix

matrix_3x3_args = ([1, 2, 3],
                   [4, 5, 6],
                   [7, 8, 9])


matrix_3x3_rotated_args = ([4, 1, 2],
                           [7, 5, 3],
                           [8, 9, 6])


matrix_4x4_args = ([1, 2, 3, 4],
                   [5, 6, 7, 8],
                   [9, 1, 2, 3],
                   [4, 5, 6, 7])


matrix_4x4_rotated_args = ([5, 1, 2, 3],
                           [9, 1, 6, 4],
                           [4, 2, 7, 8],
                           [5, 6, 7, 3])


def test_2x2_rotation():
    matrix_a = Matrix([1, 2],
                      [3, 4])
    matrix_b = Matrix([3, 1],
                      [4, 2])
    matrix_a.rotate()
    assert matrix_a == matrix_b
#
#
def test_3x3_rotation():
    matrix_a = Matrix(*matrix_3x3_args)
    matrix_b = Matrix(*matrix_3x3_rotated_args)
    matrix_a.rotate()
    assert matrix_a == matrix_b
#
#
def test_4x4_rotation():
    matrix_a = Matrix(*matrix_4x4_args)
    matrix_b = Matrix(*matrix_4x4_rotated_args)
    matrix_a.rotate()
    assert matrix_a == matrix_b


def test_shift_col_1_up():
    matrix_a = Matrix(*([1, 2, 3],
                        [4, 5, 6],
                        [7, 8, 9]))
    matrix_b = Matrix(*([4, 2, 3],
                        [7, 5, 6],
                        [7, 8, 9]))
    matrix_a.shift_col(0, 0, 2, 1)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b


def test_shift_col_1_down():
    matrix_a = Matrix(*([1, 2, 3],
                        [4, 5, 6],
                        [7, 8, 9]))
    matrix_b = Matrix(*([1, 2, 3],
                        [4, 5, 3],
                        [7, 8, 6]))
    matrix_a.shift_col(2, 0, 2, -1)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b


def test_shift_row_1():
    matrix_a = Matrix(*([1, 2, 3],
                        [4, 5, 6],
                        [7, 8, 9]))
    matrix_b = Matrix(*([1, 1, 2],
                        [4, 5, 6],
                        [7, 8, 9]))
    matrix_a.shift_row(0, 0, 2, 1)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b

def test_shift_row_2():
    matrix_a = Matrix(*([1, 2],
                        [3, 4]))
    matrix_b = Matrix(*([1, 1],
                        [3, 4]))

    matrix_a.shift_row(0, 0, 1, 1)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b
#
#
def test_shift_row_1_left():
    matrix_a = Matrix(*([1, 2, 3],
                        [4, 5, 6],
                        [7, 8, 9]))
    matrix_b = Matrix(*([1, 2, 3],
                        [4, 5, 6],
                        [8, 9, 9]))
    matrix_a.shift_row(2, 0, 2, -1)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b
#
#
def test_shift_row_2_left():
    matrix_a = Matrix(*([1, 2],
                        [3, 4]))
    matrix_b = Matrix(*([1, 2],
                        [4, 4]))
    matrix_a.shift_row(1, 0, 1, -1)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b

#
def test_shift_row_when_row_is_inner():
    matrix_a = Matrix(*([1, 2, 3, 4],
                        [5, 6, 7, 8],
                        [9, 1, 2, 3],
                        [4, 5, 6, 7]))
    matrix_b = Matrix(*([1, 2, 3, 4],
                        [5, 6, 6, 8],
                        [9, 1, 2, 3],
                        [4, 5, 6, 7]))
    matrix_a.shift_row(1, 1, 2, 1)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b
#
#
def test_shift_row_when_row_is_inner_left():
    matrix_a = Matrix(*([1, 2, 3, 4],
                        [5, 6, 7, 8],
                        [9, 1, 2, 3],
                        [4, 5, 6, 7]))
    matrix_b = Matrix(*([1, 2, 3, 4],
                        [5, 6, 7, 8],
                        [9, 2, 2, 3],
                        [4, 5, 6, 7]))
    matrix_a.shift_row(2, 1, 2, -11)
    matrix_a.data = matrix_a.new_data
    assert matrix_a == matrix_b


def test_swap_circle_1():
    matrix_a = Matrix(*([1, 2, 3],
                        [4, 5, 6],
                        [7, 8, 9]))
    matrix_b = Matrix(*([4, 1, 2],
                        [7, 5, 3],
                        [8, 9, 6]))
    matrix_a.swap_circle(1)
    assert matrix_a == matrix_b
#
#
def test_swap_2x2_circle():
    matrix_a = Matrix([1, 2],
                      [3, 4])
    matrix_b = Matrix([3, 1],
                      [4, 2])
    matrix_a.swap_circle(1)
    assert matrix_a == matrix_b
#

def test_swap_circle_1_in_4x4():
    matrix_a = Matrix(*([1, 2, 3, 4],
                        [5, 6, 7, 8],
                        [9, 1, 2, 3],
                        [4, 5, 6, 7]))
    matrix_b = Matrix(*([5, 1, 2, 3],
                        [9, 6, 7, 4],
                        [4, 1, 2, 8],
                        [5, 6, 7, 3]))
    matrix_a.swap_circle(1)
    assert matrix_a == matrix_b

def test_swap_inner_circle_in_4x4():
    matrix_a = Matrix(*(['X1',   'X2', 'X3', 'X4'],
                        ['X5',   'X6', 'X7', 'X8'],
                        ['X9',  'X10', 'X11', 'X12'],
                        ['X13', 'X14', 'X15', 'X16']))
    matrix_b = Matrix(*(['X1',   'X2', 'X3', 'X4'],
                        ['X5',  'X10', 'X6', 'X8'],
                        ['X9',  'X11', 'X7', 'X12'],
                        ['X13', 'X14', 'X15', 'X16']))
    matrix_a.swap_circle(2)
    assert matrix_a == matrix_b
