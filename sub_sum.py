import sys


def sub_sum(data):
    a = 0
    b = 0
    sums = [None] * len(data)
    not_seq_sum = None
    for idx, x in enumerate(data):
        if idx > 0:
            sums[idx] = sums[idx-1] + x
            if x > sums[idx]:
                sums[idx] = x
            if not_seq_sum + x > not_seq_sum:
                not_seq_sum += x
            if x > not_seq_sum:
                not_seq_sum = x
        else:
            sums[idx] = x
            not_seq_sum = x
    return max(sums), not_seq_sum


assert sub_sum([1, 2, 3, 4]) == (10, 10)
assert sub_sum([2, -1, 2, 3, 4, -5]) == (10, 11)
assert sub_sum([-1, -2, -3, -4, -5, -6]) == (-1, -1)
assert sub_sum([1, -1, -1, -1, -1, 5]) == (5, 6)
