def find(arr, target):
    """With given array and target number return first two members of array
    which give target number in sum"""
    accum = {}
    for x in arr:
        if accum.get(x) == None:
            accum[target - x] = x
        else:
            return (accum[x], x)


if __name__ == "__main__":
    assert find([1,3,5,4,9,7,8], 13) == (4, 9)
    assert find([1,3,5,4,9,7,8], 4) == (1, 3)
    assert find([1,3,5,4,9,7,8], 9) == (5, 4)
    assert find([1,3,5,4,9,7,8], 16) == (9, 7)
