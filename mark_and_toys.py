def game(k, data):
    data.sort()
    result = []
    while k >= 0:
        price = data.pop(0)
        if k >= price:
            result.append(price)
            k -= price
        else:
            break
    return len(result)

assert game(50, [1, 12, 5, 111, 200, 1000, 10]) == 4