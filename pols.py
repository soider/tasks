import sys

SQUARE = 1
RECTANGLE = 2
OTHER = 3


def polygon(a, b, c, d):
    if len([coord for coord in [a, b, c, d] if coord <= 0]):
        return OTHER
    if a == b == c == d:
        return SQUARE
    else:
        if a == c and d == b:
            return RECTANGLE
    return OTHER


if __name__ == "__main__":
    data = [map(int, line.split()) for line
            in sys.stdin.read().strip().split('\n')]
    total_squares, total_rects, total_other = 0, 0, 0
    for a, b, c, d in data:
        figure_type = polygon(a, b, c, d)
        if figure_type == SQUARE:
            total_squares += 1
        elif figure_type == RECTANGLE:
            total_rects += 1
        else:
            total_other += 1
    print "{0} {1} {2}".format(total_squares, total_rects, total_other)
