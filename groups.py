#coding: utf-8
"""Проверить скобочную последовательность на правильность"""

closes = {
    '{': '}',
    '(': ')',
    '[': ']'
}


def is_opener(c):
    return c in closes.keys()


def is_closer(c):
    return c in closes.values()


def group_check(s):
    stack = []
    for symbol in s:
        if is_opener(symbol):
            stack.append(symbol)
        elif is_closer(symbol):
            if not stack:
                return False
            prev = stack.pop()
            if closes[prev] == symbol:
                continue
            else:
                return False
        else:
            return False
    if not stack:
        return True
    return False
