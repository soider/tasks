def _klongest(seq):
    x = len(seq)
    result = ""
    counter = set()
    for j in range(1, x+1):
        for i in range(j, x+1):
            subseq = seq[j:i]
            if len(set(subseq)) <= 2 and len(subseq) > len(result):
                    result = subseq
    return result

def klongest(seq):
    x = len(seq)
    result = ""
    counter = set()
    for i in range(x+1):
        for j in range(1, x+1):
            print i, j 
    return result

assert klongest("zeceba") == "ece"
