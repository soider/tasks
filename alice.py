def game_brute(data):
    result = [1] * len(data)
    for i, rating in enumerate(data):
        if i == 0:
            continue
        if data[i-1] > data[i]:
            if result[i - 1] <= result[i]:
                result[i - 1] = result [i] + 1
                for j in range(i-1, -1, -1):
                    if data[j] > data[j+1]:
                        if result[j] <= result[j+1]:
                            result[j] = result[j+1] + 1
        elif data[i-1] < data[i]:
            if result[i - 1] >= result[i]:
                result[i] = result[i-1] + 1 # always 2?
            else:
                assert False, "Never should got there"
    return sum(result)


def game_3(data):
    result = [0] * len(data)
    for idx, el in enumerate(data):
        if idx == 0:
            result[idx] = 1
            continue
        if el > data[idx-1]:
            result[idx] = result[idx-1] + 1
        else:
            result[idx] = 1
    data = list(reversed(data))
    result_2 = [0] * len(data)
    for idx, el in enumerate(data):
        if idx == 0:
            result_2[idx] = 1
            continue
        if el > data[idx-1]:
            result_2[idx] = result_2[idx-1] + 1
        else:
            result_2[idx] = 1
    
    print result, result_2
    return sum(result) + sum(result_2)


def brutforce():
    import random 
    while True:
        data = []
        l = random.randint(1, 10)
        for _ in range(l):
            data.append(random.randint(1, 9))
        a = game_brute(data)  
        b = game_3(data)
        assert a == b, (data, a,b)
        
print game_brute([1, 1, 7, 1, 2, 5, 9, 7, 1, 7])
print game_3([1, 1, 7, 1, 2, 5, 9, 7, 1, 7])
# print game_2([2,4,2,6,1,7,8,9,2,1])
brutforce()
# assert game_brute([1,2,2]) == 4
# assert game_brute([2,4,2,6,1,7,8,9,2,1]) == 19

# n = int(raw_input())
# data = []
# for _ in range(n):
#     data.append(int(raw_input()))
# print game(data)
