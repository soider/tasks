import collections
import string


def normalize_word(word):
    return word.strip().strip(string.punctuation).lower()


def tokens(raw_data):
    return map(normalize_word, raw_data.split())


def build_rating(keywords, reviews):
    keywords = set(keywords)

    hotel_weights = collections.defaultdict(int)
    for hotel_id, hotel_reviews in reviews.items():
        hotel_counter = collections.defaultdict(int)
        for review in hotel_reviews:
            review_words = tokens(review)
            interesting_words = [word for word in review_words
            if word in keywords]
            counter = collections.Counter(interesting_words)
            weight = sum(x for x in counter.values())
            hotel_weights[hotel_id] += weight
    return hotel_weights

def print_rating(rating):
    sorted_hotels = sorted(rating.items(),
                            key=lambda el: (el[1], el[0]),
                            reverse=True)
    print " ".join(str(hotel_id) for hotel_id, _ in sorted_hotels)

if __name__ == "__main__":
    words = tokens(raw_input())
    count = int(raw_input())
    data = collections.defaultdict(list)
    for _ in range(count):
        hotel_id = int(raw_input())
        review = raw_input()
        data[hotel_id].append(review)

    rating = build_rating(words, data)
    print_rating(rating)
