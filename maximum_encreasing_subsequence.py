# coding: utf-8
"""Для каждого члена исходной последовательности нужно вычислить
максимальную длину возрастающей подпоследовательности,
окончивающейся этим членом"""

from collections import defaultdict


def max_subsequence(seq):
    """Каждому i элементу соответствует как минимум
    подпоследовательность из одного этого элемента.
    Так же можно составить подпоследовательность
    из любой подпоследовательность для i-1 элемента + текущий элемент.
    Тогда максимальная длина подпоследовательности для i-го элемента =
    max(max_lengths[i], max_length[i-1] + 1)
    """
    max_lengths = [0] * len(seq)
    for idx, element in enumerate(seq):
        max_lengths[idx] = 1
        for inner_idx in range(idx): # обход каждого из предыдущих элементов
            if seq[inner_idx] < element:
                max_lengths[idx] = max(max_lengths[idx],
                                       max_lengths[inner_idx] + 1)
    return max_lengths


if __name__ == "__main__":
    assert max_subsequence([2, 5, 3, 4, 6, 1]) == [1, 2, 2, 3, 4, 1]
