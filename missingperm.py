"""
A zero-indexed array A consisting of N different integers is given. The array contains integers in the range [1..(N + 1)], which means that exactly one element is missing.
Your goal is to find that missing element.
"""


def solution(array):
    sum = 0
    expected_sum = 0
    for el in array:
        sum += el
    for el in range(1, len(array) + 2):
        expected_sum += el
    return abs(sum-expected_sum)


if __name__ == "__main__":
    assert solution([2,3,1,5]) == 4
