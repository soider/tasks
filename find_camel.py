def camel(inp):
    l = len(filter(lambda el: el.isupper(), inp))
    if l > 0:
        return l + 1
    return 0

assert camel("saveChangesInTheEditor") == 5

print camel("ASD")
