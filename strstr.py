def strstr(haystack, needle):
    if haystack == needle:
        return 0
    needle_length = len(needle)
    haystack_length = len(haystack)
    found = False
    for i in range(haystack_length):
        for j in range(needle_length):
            if needle[j] != haystack[i+j]:
                break
            found = True
        if found:
            return i
    return -1


assert strstr("Mom", "Mom") == 0
assert strstr("Mom", "om") == 1
assert strstr("Momo", "mo") == 2
assert strstr("Mom", "zih") == -1
