def knapsack(capacity, weights, values):
    matrix = [[0] * (capacity+1) for _ in range(len(weights))]
    for max_cargo_idx in range(len(weights)):
        for current_capacity in range(1, capacity + 1):
            if current_capacity == 0:
                continue
            current_cargo_weight = weights[max_cargo_idx]
            current_cargo_value = values[max_cargo_idx]
            if max_cargo_idx == 0: # checking first cargo
                if current_capacity >= current_cargo_weight:
                    matrix[max_cargo_idx][current_capacity] = current_cargo_value
                else:
                    matrix[max_cargo_idx][current_capacity] = 0
                continue
            max_for_previous_cargo = matrix[max_cargo_idx-1][current_capacity]
            if current_cargo_weight > current_capacity:
                matrix[max_cargo_idx][current_capacity] = max_for_previous_cargo 
                continue
            remained_capacity = current_capacity - current_cargo_weight
            new_value = max(max_for_previous_cargo, current_cargo_value + matrix[max_cargo_idx-1][remained_capacity])
            matrix[max_cargo_idx][current_capacity] = new_value
    return matrix[max_cargo_idx][capacity]


assert knapsack(4, [4,5,1], [1,2,3]) == 3
assert knapsack(7, [1,3,4,5], [1,4,5,7]) == 9