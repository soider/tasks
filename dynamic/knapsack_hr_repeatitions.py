def game(capacity, values):
    capacity = capacity+1
    value = [0] * capacity
    for w in range(capacity):
        for i, v in enumerate(values):
            if v <= w:
                val = value[w - v] + v
                if val > value[w]:
                    value[w] = val
    return value[capacity-1]
    

assert game(12, [1,6,9]) == 12
assert game(9, [3,4,4,4,8]) == 9

assert game(6, [5]) == 5
assert game(8, [3, 3, 3, 3, 3, 3]) == 6
assert game(10, [9, 4, 4, 9, 4, 9, 9, 9, 9]) == 9
