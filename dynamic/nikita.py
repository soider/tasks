def game(data):
    print data
    if len(data) < 2:
        return 0
    if len(data) == 2:
        if data[0] == data[1]:
            return 1
    n = len(data)
    left_sum = data[0]
    right_sum = sum(data[1:])
    border = None
    if left_sum == right_sum:
        return 1 + game(data[1:])
    for i in range(1, n):
        left_sum += data[i]
        right_sum -= data[i]
        if left_sum == right_sum:
            border = i+1
            break
    if border is None:
        return 0
    left_score = game(data[:border])
    right_score = game(data[border:])
    return 1 + max(left_score, right_score)

def game_d(data):
    values = [[0] * len(data) for _ in range(len(data))]
    for left in range(len(values)):
        for right in range(len(values[0])):
            pass


print game_d([3,3,3])
assert game([3,3,3]) == 0
assert game([2, 1, 1]) == 2
assert game([2,2,2,2]) == 2
assert game([4,1,0,1,1,0,1]) == 3
