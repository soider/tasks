def reduce_string(inp):
    inp = list(inp)
    swapped = True
    while swapped:
        swapped = False
        length = len(inp)
        if length == 2 and inp[1] == inp[0]:
            return "Empty String"
        for i in range(length):
            if i == length - 1:
                break
            if inp[i] == inp[i+1]:
                swapped = True
                inp.pop(i+1)
                inp.pop(i)
                break
    return "".join(inp)


assert reduce_string("aaabccddd") == "abd"
assert reduce_string("baab") == "Empty String"
assert reduce_string("aa") == "Empty String"
assert reduce_string("kagoyzkgfjnyvjewazalxngpdcfahneqoqgiyjgpzobhaghmgzmnwcmeykqzgajlmuerhhsanpdtmrzibswswzjjbjqytgfewiuu") == "kagoyzkgfjnyvjewazalxngpdcfahneqoqgiyjgpzobhaghmgzmnwcmeykqzgajlmuersanpdtmrzibswswzbjqytgfewi"
